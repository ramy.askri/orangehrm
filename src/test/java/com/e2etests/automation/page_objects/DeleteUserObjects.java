package com.e2etests.automation.page_objects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.e2etests.automation.utils.Setup;

public class DeleteUserObjects {
	
	// Constructeur
	public DeleteUserObjects() {
		PageFactory.initElements(Setup.getDriver(), this);
	}
	
	
	// -----------------------------------------------------------------------
	// /!\                      Variables & WebElements                     
	// -----------------------------------------------------------------------	
	@FindBy (how = How.CSS, using = ".oxd-table-card button:nth-child(1)")
	public static WebElement delete_button;
	
	@FindBy (how = How.CLASS_NAME, using = "oxd-button--label-danger")
	public static WebElement delete_button_confirmation;
	
	// -----------------------------------------------------------------------
	// /!\                      Méthodes                    
	// -----------------------------------------------------------------------	
	
	public void deleteButtonClick() {
		delete_button.click();
	}
	
	public void deleteButtonConfirmationClick() {
		WebDriverWait wait_delete_button_confirmation = new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait_delete_button_confirmation.until(ExpectedConditions.elementToBeClickable(DeleteUserObjects.delete_button_confirmation));
		
		delete_button_confirmation.click();
	}
}
