package com.e2etests.automation.page_objects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.e2etests.automation.utils.Setup;

public class FindUserByUsernameObjects {

	// Constructeur
	public FindUserByUsernameObjects() {
		PageFactory.initElements(Setup.getDriver(), this);
	}
	
	// -----------------------------------------------------------------------
	// /!\                      Variables & WebElements                     
	// -----------------------------------------------------------------------	
	
	// Déclaration des WebElement
	@FindBy (how = How.CSS, using = ".oxd-table-filter .oxd-grid-item:nth-child(1) input")
	public static WebElement username_field;
	
	@FindBy (how = How.CSS, using = ".oxd-form-actions button[type='submit']")
	public static WebElement search_button;
	
	@FindBy (how = How.CSS, using = ".oxd-table-card .oxd-table-cell:nth-child(2) div")
	public static WebElement search_result;
	
	// -----------------------------------------------------------------------
	// /!\                      Méthodes                     
	// -----------------------------------------------------------------------	
	
	public void fillUsername(String username) {
		WebDriverWait wait_username_field = new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait_username_field.until(ExpectedConditions.elementToBeClickable(FindUserByUsernameObjects.username_field));
		
		username_field.clear();
		username_field.sendKeys(username);
	}
	
	public void searchButtonClick() {
		search_button.click();
	}
	
}
