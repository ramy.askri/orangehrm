package com.e2etests.automation.page_objects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.e2etests.automation.utils.Setup;

public class AddUserObjects {
	
	// Constructeur
	public AddUserObjects() {
		PageFactory.initElements(Setup.getDriver(), this);
	}
	
	// -----------------------------------------------------------------------
	// /!\                      Variables & WebElements                     
	// -----------------------------------------------------------------------
	
	// Déclaration des variables
	
	// Déclaration des WebElement
	@FindBy (how = How.CSS, using = ".oxd-main-menu li:first-child a")
	public static WebElement admin_link;
	
	@FindBy (how = How.CSS, using = ".orangehrm-paper-container .oxd-button")
	public static WebElement add_button;
	
	@FindBy (how = How.CSS, using = ".oxd-grid-item:nth-child(1) .oxd-select-wrapper")
	public static WebElement role_select;
	
	@FindBy (how = How.CSS, using = ".oxd-select-dropdown div:nth-child(3)")
	public static WebElement ess_role;
	
	@FindBy (how = How.CSS, using = ".oxd-grid-item:nth-child(2) input")
	public static WebElement employee_name_field;
	
	@FindBy (how = How.CSS, using = ".oxd-autocomplete-dropdown div:nth-child(2)")
	public static WebElement employee_name_choice;
	
	@FindBy (how = How.CSS, using = ".oxd-grid-item:nth-child(3) .oxd-select-wrapper")
	public static WebElement status_select;
	
	@FindBy (how = How.CSS, using = ".oxd-select-dropdown div:nth-child(2)")
	public static WebElement enabled_status;
	
	@FindBy (how = How.CSS, using = ".oxd-grid-item:nth-child(4) input")
	public static WebElement username_field;
	
	@FindBy (how = How.CSS, using = ".user-password-cell input")
	public static WebElement password_field;
	
	@FindBy (how = How.CSS, using = ".user-password-row .oxd-grid-item:nth-child(2) input")
	public static WebElement confirm_password_field;
	
	@FindBy (how = How.CSS, using = "button[type=submit]")
	public static WebElement submit_button;
	
	@FindBy (how = How.ID, using = "oxd-toaster_1")
	public static WebElement success_toast;
	
	@FindBy (how = How.CSS, using = "#oxd-toaster_1 p+p")
	public static WebElement success_toast_text;
	
	// -----------------------------------------------------------------------
	// /!\                      Méthodes                    
	// -----------------------------------------------------------------------
	public void AdminLinkClick() {
		admin_link.click();
	}
	
	public void AddButtonClick() {
		add_button.click();
	}
	
	public void userRoleChoice() {
		WebDriverWait wait_role_select = new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait_role_select.until(ExpectedConditions.elementToBeClickable(AddUserObjects.role_select));
		
		role_select.click();
		ess_role.click();
	}
	
	public void fillEmployeeName (String name){
		employee_name_field.clear();
		employee_name_field.sendKeys(name);
		
		WebDriverWait wait_employee_name_choice = new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait_employee_name_choice.until(ExpectedConditions.elementToBeClickable(AddUserObjects.employee_name_choice));
		employee_name_choice.click();
	}
	
	public void statuEnabledChoice() {
		status_select.click();
		enabled_status.click();
	}
	
	public void fillUsername(String new_username) {
		username_field.clear();
		username_field.sendKeys(new_username);
	}
	
	public void fillPassword(String user_password) {
		password_field.clear();
		password_field.sendKeys(user_password);
	}
	
	public void fillConfirmPassword(String confirm_password) {
		confirm_password_field.clear();
		confirm_password_field.sendKeys(confirm_password);
	}
	
	public void saveClick() {
		submit_button.click();
	}
	
	
}
