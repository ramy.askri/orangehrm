package com.e2etests.automation.page_objects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.e2etests.automation.utils.ConfigFileReader;
import com.e2etests.automation.utils.Setup;

public class LoginObjects {
	
	// Constructeur
	public LoginObjects() {
		configFileReader = new ConfigFileReader();
		PageFactory.initElements(Setup.getDriver(), this);
	}
	
	// -----------------------------------------------------------------------
	// /!\                      Variables & WebElements                     
	// -----------------------------------------------------------------------
	
	// Déclaration des variables
	public ConfigFileReader configFileReader;
	
	// Déclaration des WebElement
	@FindBy (how = How.CSS, using = "input[name='username']")
	public static WebElement usernameField;
	
	@FindBy (how = How.CSS, using = "input[name='password']")
	public static WebElement passwordField;
	
	@FindBy (how = How.CSS, using = "button[type='submit']")
	public static WebElement submitButton;
	
	@FindBy (how = How.CSS, using = "aside.oxd-sidepanel")
	public static WebElement navigation;
	
	@FindBy (how = How.CLASS_NAME, using = "oxd-alert--error")
	public static WebElement alert_error;
	
	@FindBy (how = How.CSS, using = ".oxd-alert--error p")
	public static WebElement alert_error_msg;
	
	
	// -----------------------------------------------------------------------
	// /!\                      Méthodes                      
	// -----------------------------------------------------------------------
	
	// Remplir le champ Username
	public void fillUsername(String username) {
		// Attendre jusqu'à ce que le champ Username s'affiche sur la page
		WebDriverWait wait=new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait.until(ExpectedConditions.elementToBeClickable(usernameField));
		
		usernameField.clear();
		usernameField.sendKeys(username);
	}
	
	// Remplir le champ Password
	public void fillPassword(String password) {
		// Attendre jusqu'à ce que le champ Password s'affiche sur la page
		WebDriverWait wait=new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait.until(ExpectedConditions.elementToBeClickable(usernameField));
		
		passwordField.clear();
		passwordField.sendKeys(password);
	}
	
	// Cliquer sur le bouton Submit
	public void clickSubmitButton() {
		submitButton.click();
	}
	
	// Connexion à l'application OrangeHRM
	public void connectToOrangeHRM() {
		Setup.getDriver().get(configFileReader.getProperties("authentificationURL"));
		fillUsername(configFileReader.getProperties("username"));
		fillPassword(configFileReader.getProperties("password"));
		clickSubmitButton();
	}
	
}
