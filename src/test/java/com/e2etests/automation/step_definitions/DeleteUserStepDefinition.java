package com.e2etests.automation.step_definitions;

import com.e2etests.automation.page_objects.DeleteUserObjects;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;

public class DeleteUserStepDefinition {
	
	DeleteUserObjects deleteUserObjects;
	
	// Constructeur
	public DeleteUserStepDefinition() {
		deleteUserObjects = new DeleteUserObjects();
	}

	@When("je clique sur bouton Corbeille")
	public void jeCliqueSurBoutonCorbeille() {
		deleteUserObjects.deleteButtonClick();
	}
	
	@And("je clique sur le bouton Yes, Delete")
	public void jeCliqueSurLeBoutonYesDelete() {
		deleteUserObjects.deleteButtonConfirmationClick();
	}
	
}
