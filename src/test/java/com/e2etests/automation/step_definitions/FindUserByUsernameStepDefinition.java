package com.e2etests.automation.step_definitions;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.e2etests.automation.page_objects.FindUserByUsernameObjects;
import com.e2etests.automation.utils.Setup;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class FindUserByUsernameStepDefinition {
	
	FindUserByUsernameObjects findUserByUsernameObjects;
	
	// Constructeur
	public FindUserByUsernameStepDefinition() {
		findUserByUsernameObjects = new FindUserByUsernameObjects();
	}

	@When("je saisi {string} dans le champ Username du formulaire de recherche")
	public void jeSaisiDansLeChampUsernameDuFormulaireDeRecherche(String username) {
		findUserByUsernameObjects.fillUsername(username);
	}
	
	@And("je clique sur bouton Search")
	public void jeCliqueSurBoutonSearch() {
		findUserByUsernameObjects.searchButtonClick();
	}
	
	@Then("l'utilsiateur {string} s'affiche dans le tableau Record")
	public void lUtilsiateurSAfficheDansLeTableauRecord(String user) {
		
		WebDriverWait wait_search_result = new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait_search_result.until(ExpectedConditions.elementToBeClickable(FindUserByUsernameObjects.search_result));
		
		String searchResult = FindUserByUsernameObjects.search_result.getText();
	    Assert.assertEquals(user, searchResult);
	}

}
