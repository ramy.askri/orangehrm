package com.e2etests.automation.step_definitions;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.e2etests.automation.page_objects.LoginObjects;
import com.e2etests.automation.utils.Setup;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStepDefinition {
	
	LoginObjects loginObjects;
	
	// Constructeur
	public LoginStepDefinition() {
		loginObjects = new LoginObjects();
	}
	

	// Cas passant
	@Given("je me connecte a l'application OrangeHRM")
	public void jeMeConnecteALApplicationOrangeHRM() {
		loginObjects.connectToOrangeHRM();
	}
	
	@Then("le Dashboard s'ouvre")
	public void leDashboardSOuvre() {
		// On vérifie d'abord si la redirection vers le Dashboard est effectuée
		String currentURL = Setup.getDriver().getCurrentUrl();
		Assert.assertEquals("https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index", currentURL);
		
		// On vérifie la présence d'un élément du Dashboard sur la page
		WebDriverWait wait=new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait.until(ExpectedConditions.elementToBeClickable(LoginObjects.navigation));
		Assert.assertTrue(LoginObjects.navigation.isDisplayed());
	}
	
	
	// Cas non passant
	@Given("j'ouvre la page de connexion de OrangeHRM")
	public void jOuvreLaPageDeConnexionDeOrangeRHM() {
		Setup.getDriver().get(loginObjects.configFileReader.getProperties("authentificationURL"));
	}
	
	@When("je saisi le username {string}")
	public void jeSaisiLeUsername(String username) {
		loginObjects.fillUsername(username);
	}
	
	@When("je saisi le mot de passe {string}")
	public void jeSaisiLeMotDePasse(String password) {
		loginObjects.fillPassword(password);
	}
	
	@When("je clique sur le bouton Login")
	public void jeCliqueSurLeBoutonLogin() {
		loginObjects.clickSubmitButton();
	}
	
	@Then("le message d'erreur {string} s'affiche")
	public void leMessageDErreurSAffiche(String error_msg) {
		// Attendre jusqu'à ce que le message d'erreur s'affiche
		WebDriverWait wait=new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait.until(ExpectedConditions.elementToBeClickable(LoginObjects.alert_error));
		
		// Vérifier si l'alert d'erreur s'affiche
	    Assert.assertTrue(LoginObjects.alert_error.isDisplayed());
	    
	    // Vérifier si le message d'erreur est correcte		
	    String current_error_msg = LoginObjects.alert_error_msg.getText();
	    Assert.assertEquals(error_msg, current_error_msg);
	}

}
