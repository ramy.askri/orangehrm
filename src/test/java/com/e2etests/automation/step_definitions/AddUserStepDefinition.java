package com.e2etests.automation.step_definitions;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.e2etests.automation.page_objects.AddUserObjects;
import com.e2etests.automation.utils.Setup;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AddUserStepDefinition {
	
	AddUserObjects addUserObjects;
	
	// Constructeur
	public AddUserStepDefinition() {
		addUserObjects = new AddUserObjects();
	}

	@When("je clique sur le lien Admin dans la Nav bar à gauche")
	public void jeCliqueSurLeLienAdminDansLaNavBarÀGauche() {
		addUserObjects.AdminLinkClick();
	}
	
	@And("la page User Management s'ouvre")
	public void laPageUserManagementSOuvre() {
		WebDriverWait wait_add_button = new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait_add_button.until(ExpectedConditions.elementToBeClickable(AddUserObjects.add_button));
	    Assert.assertTrue(AddUserObjects.add_button.isDisplayed());
	}
	
	@And("je clique sur le bouton +Add")
	public void jeCliqueSurLeBoutonAdd() {
		addUserObjects.AddButtonClick();
	}
	
	@And("je selectionne le rôle ESS dans le champ select User Role")
	public void jeSelectionneLeRôleESSDansLeChampSelectUserRole() {
		addUserObjects.userRoleChoice();
	}
	
	@And("je saisi {string} dans le champ Employee Name")
	public void jeSaisiDansLeChampEmployeeName(String name) {
		addUserObjects.fillEmployeeName(name);
	}
	
	@And("je selectionne le statu Enabled dans le champ select Status")
	public void jeSelectionneLeStatuEnabledDansLeChampSelectStatus() {
		addUserObjects.statuEnabledChoice();
	}
	
	@And("je saisi {string} dans le champ Username")
	public void jeSaisiDansLeChampUsername(String new_username) {
		addUserObjects.fillUsername(new_username);
	}
	
	@And("je saisi {string} dans le champ Password")
	public void jeSaisiDansLeChampPassword(String user_password) {
		addUserObjects.fillPassword(user_password);
	}
	
	@And("je saisi {string} dans le champ Confirm Password")
	public void jeSaisiDansLeChampConfirmPassword(String confirm_password) {
		addUserObjects.fillConfirmPassword(confirm_password);
	}
	
	@And("je clique sur bouton Save")
	public void jeCliqueSurBoutonSave() {
		addUserObjects.saveClick();
	}
	
	@Then("le message {string} s'affiche")
	public void leMessageDeConfirmationSAffiche(String success_msg) {
		WebDriverWait wait_success_toast = new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait_success_toast.until(ExpectedConditions.elementToBeClickable(AddUserObjects.success_toast));
		
		Assert.assertTrue(AddUserObjects.success_toast.isDisplayed());
		String msg = AddUserObjects.success_toast_text.getText();
		Assert.assertEquals(msg, success_msg);
	}

}
