@Delete-User
Feature: Test de suppression d'un utilisateur
  En tant qu'admin je souhaite supprimer un utilisateur existant

  @delete-user
  Scenario: Je souhaite tester la suppression d'un utilisateur
    Given je me connecte a l'application OrangeHRM
    When le Dashboard s'ouvre
    And je clique sur le lien Admin dans la Nav bar à gauche
    And la page User Management s'ouvre
    And je saisi "the_user_name" dans le champ Username du formulaire de recherche
    And je clique sur bouton Search
    Then l'utilsiateur "the_user_name" s'affiche dans le tableau Record
    And je clique sur bouton Corbeille
    And je clique sur le bouton Yes, Delete
    Then le message "Successfully Deleted" s'affiche