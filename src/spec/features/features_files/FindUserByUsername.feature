@Find-User
Feature: Test de recherche d'un utilisateur par son Username
  En tant qu'admin je souhaite cherche un utilisateur par son Username

  @Find-User-By-Username
  Scenario: Je souhaite tester la recherche d'un utilisateur par son Username
    Given je me connecte a l'application OrangeHRM
    When le Dashboard s'ouvre
    And je clique sur le lien Admin dans la Nav bar à gauche
    And la page User Management s'ouvre
    And je saisi "the_user_name" dans le champ Username du formulaire de recherche
    And je clique sur bouton Search
    Then l'utilsiateur "the_user_name" s'affiche dans le tableau Record