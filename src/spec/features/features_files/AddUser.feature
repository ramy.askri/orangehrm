@Add-User
Feature: Test d'ajout d'un nouveau utilisateur
  En tant qu'admin je souhaite ajouter un nouveau utilisateur de type ESS

  @Add-User-Cas-Passant
  Scenario: Je souhaite tester l'ajout d'un utilisateur de type ESS
    Given je me connecte a l'application OrangeHRM
    When le Dashboard s'ouvre
    And je clique sur le lien Admin dans la Nav bar à gauche
    And la page User Management s'ouvre
    And je clique sur le bouton +Add
    And je selectionne le rôle ESS dans le champ select User Role
    And je saisi "a" dans le champ Employee Name
    And je selectionne le statu Enabled dans le champ select Status
    And je saisi "the_user_name" dans le champ Username
    And je saisi "pwd1234" dans le champ Password
    And je saisi "pwd1234" dans le champ Confirm Password
    And je clique sur bouton Save
    Then le message "Successfully Saved" s'affiche