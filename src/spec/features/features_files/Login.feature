@Login
Feature: test de la page d authentification
  ETQ utilisateur je souhaite tester la page de la connexion de l application OrangeHRM

  @Connexion-Cas-Passant
  Scenario: Je souhaite tester la page de la connexion avec un cas passant
    Given je me connecte a l'application OrangeHRM
    Then le Dashboard s'ouvre
    
    
  @Connexion-Cas-Non-Passant-1
  Scenario: Je souhaite tester la page de la connexion avec un bon username et un faut password
    Given j'ouvre la page de connexion de OrangeHRM
    When je saisi le username "Admin"
    And je saisi le mot de passe "123"
    And je clique sur le bouton Login
    Then le message d'erreur "Invalid credentials" s'affiche
    
    
  @Connexion-Cas-Non-Passant-2
  Scenario: Je souhaite tester la page de la connexion avec un faut username et un bon password
    Given j'ouvre la page de connexion de OrangeHRM
    When je saisi le username "test"
    And je saisi le mot de passe "admin123"
    And je clique sur le bouton Login
    Then le message d'erreur "Invalid credentials" s'affiche
    
    
  @Connexion-Cas-Non-Passant-3
  Scenario: Je souhaite tester la page de la connexion avec un faut username et un faut password
    Given j'ouvre la page de connexion de OrangeHRM
    When je saisi le username "test"
    And je saisi le mot de passe "123"
    And je clique sur le bouton Login
    Then le message d'erreur "Invalid credentials" s'affiche